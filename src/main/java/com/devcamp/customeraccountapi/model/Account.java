package com.devcamp.customeraccountapi.model;

public class Account {
    
    private int id;
    private Customer customer ;
    private double balance = 0.0;
    
     public Account(int id, double balance) {
         this.id = id;
         this.balance = balance;
     }
 
     public Account(int id, Customer customer, double balance) {
         this.id = id;
         this.customer = customer;
         this.balance = balance;
     }
 
     public Account() {
     }
 
     public int getId() {
         return id;
     }
 
     public Customer getCustomer() {
         return customer;
     }
 
     public double getBalance() {
         return balance;
     }
     
 
     public void setCustomer(Customer customer) {
         this.customer = customer;
     }
 
     public void setBalance(double balance) {
         this.balance = balance;
     }
 
     
     public double deposit(double amount){
         return balance += amount ;
     }
 
     public double withdraw(double amount){
         return balance -= amount ;
     }
 
     @Override
     public String toString() {
         return "Account [balance=" + balance + ", customer=" + customer + ", id=" + id + "]";
     }
 
     
 
}

