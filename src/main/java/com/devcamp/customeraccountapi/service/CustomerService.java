package com.devcamp.customeraccountapi.service;


import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccountapi.model.Customer;;

@Service
public class CustomerService {
    Customer cus1 = new Customer(1, "Thuan", 10);
    Customer cus2 = new Customer(2, "Toan", 20);
    Customer cus3 = new Customer(3, "Hung", 30);

    public ArrayList<Customer> customerList(){
        ArrayList<Customer> cusList = new ArrayList<>();

        cusList.add(cus1);
        cusList.add(cus2);
        cusList.add(cus3);

        return cusList ;
        
    }

}

