package com.devcamp.customeraccountapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountapi.model.Account;
import com.devcamp.customeraccountapi.service.AccountService;


@RestController
public class AccountController {
    
    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts")
    public ArrayList<Account> getAccount(){
        ArrayList<Account> accounts = accountService.getAccountList();
        return accounts ;
    }
    
}
