package com.devcamp.customeraccountapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountapi.model.Customer;
import com.devcamp.customeraccountapi.service.CustomerService;

@RestController
public class CustomerController {
    
        @Autowired
        private CustomerService customerService;

        @GetMapping("/customers")
        public ArrayList<Customer> getCustomer(){
            ArrayList<Customer> allCus = customerService.customerList();
            return allCus ;
        }
    
}
